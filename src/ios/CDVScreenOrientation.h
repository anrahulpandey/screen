/********* CDVScreenOrientation.h Cordova Plugin Header *******/

#import <Cordova/CDV.h>

@interface CDVScreenOrientation : CDVPlugin

- (void) showStatusBar:(CDVInvokedUrlCommand*)command;

- (void) hideStatusBar:(CDVInvokedUrlCommand*)command;

@end
