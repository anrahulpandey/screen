/********* CDVScreenOrientation.m Cordova Plugin Implementation *******/

#import "CDVScreenOrientation.h"
#import "AppDelegate.h"
#import <Cordova/CDV.h>

@implementation CDVScreenOrientation
BOOL value;

- (void) hideStatusBar:(CDVInvokedUrlCommand *)command
{
    NSLog(@"Hide Stauts bar plugin");
    CDVPluginResult* pluginResult = nil;
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        value=YES;
        NSLog(@"Inside show selector Value = %hhd",value);
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        NSLog(@"hide iOS6");
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    }
    
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) showStatusBar:(CDVInvokedUrlCommand *)command
{
    NSLog(@"Show Status Bar");
    CDVPluginResult* pluginResult = nil;
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        value=NO;
        NSLog(@"Inside show selector Value = %hhd",value);
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        NSLog(@"show iOS6");
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    
    // [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (BOOL)prefersStatusBarHidden
{
    NSLog(@"Value = %hhd",value);
    return value;
}

@end
