var cordova = require('cordova'),
    exec = require('cordova/exec');

var ScreenOrientation = function() {
        this.options = {};
};

ScreenOrientation.prototype = {
    /*
        Add your plugin methods here
    */
    showstatus: function() {
        cordova.exec(null, null, "ScreenOrientation", "showStatusBar", []);
    }
    
    hidestatus: function() {
        cordova.exec(null, null, "ScreenOrientation", "hideStatusBar", []);
    }
};

var ScreenOrientationInstance = new ScreenOrientation();

module.exports = ScreenOrientationInstance;